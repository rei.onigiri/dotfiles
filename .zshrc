# Start tmux on startup
if [[ -z "$INTELLIJ_ENVIRONMENT_READER" ]] && command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
  exec tmux new-session -A -s main
fi

# Load common modules
for f in $(ls -d $SO_ZSH_HOME/common/*); do source $f; done

# Load modules based on OS
if [[ $OSTYPE == 'darwin'* ]]; then
    for f in $(ls -d $SO_ZSH_HOME/macos/*); do source $f; done
else
    for f in $(ls -d $SO_ZSH_HOME/linux/*); do source $f; done
fi

# Load local modules
if [[ -d $SO_LOCAL_ZSH_HOME ]]; then
    for f in $(ls -d $SO_LOCAL_ZSH_HOME/*); do source $f; done 
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
if [[ -d $SDKMAN_DIR ]]; then
    [[ -s "$SDKMAN_DIR/bin/sdkman-init.sh" ]] && source "$SDKMAN_DIR/bin/sdkman-init.sh"
fi

