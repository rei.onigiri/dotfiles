set -g prefix C-Space

set -g mode-keys vi
set -g default-terminal "screen-256color"
setw -g xterm-keys on
set -s escape-time 10                     # faster command sequences
set -sg repeat-time 600                   # increase repeat timeout
set -s focus-events on
set -q -g status-utf8 on                  # expect UTF-8 (tmux < 2.2)
setw -q -g utf8 on
set -g history-limit 5000                 # boost history
set -g set-clipboard on
set -g base-index 1           # start windows numbering at 1
setw -g pane-base-index 1     # make pane numbering consistent with windows
setw -g automatic-rename on   # rename window to reflect current program
set -g renumber-windows on    # renumber windows when a window is closed
set -g set-titles on          # set terminal title
set -g display-panes-time 800 # slightly longer pane indicators display time
set -g display-time 1000      # slightly longer status messages display time
set -g status-interval 10     # redraw status line every 10 seconds
set -g mouse on

# reload configuration
bind r source-file ~/.tmux.conf \; display '~/.tmux.conf sourced'

# session navigation
bind BTab switch-client -l  # move to last session

################################################################################
##
## PLUGINS
##
################################################################################

set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'dracula/tmux'
set -g @plugin 'IngoMeyer441/tmux-easy-motion'

################################################################################
##
## SOURCE FILES
##
################################################################################

source-file "$SO_TMUX_HOME/*.conf"

run '~/.tmux/plugins/tpm/tpm'
