export SO_DOTFILES_HOME=$HOME/.dotfiles
export SO_ZSH_HOME=$SO_DOTFILES_HOME/.zsh.d
export SO_LOCAL_ZSH_HOME=$HOME/.local.zsh.d
export SO_VIM_HOME=$SO_DOTFILES_HOME/.vim.d
export SO_TMUX_HOME=$SO_DOTFILES_HOME/.tmux.conf.d

if [[ -d "$HOME/.cargo" ]]; then
    . "$HOME/.cargo/env"
fi

if [[ -f "$HOME/.zshenv.local" ]]; then
    . "$HOME/.zshenv.local"
fi
