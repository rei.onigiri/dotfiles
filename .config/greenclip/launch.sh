#!/bin/bash

CONFIG_FILES="$HOME/.config/greenclip.toml"

trap "killall greenclip" EXIT

echo "---" | tee -a /tmp/greenclip.log 

while true; do
    greenclip daemon 2>&1 | tee -a /tmp/greenclip.log & disown
    inotifywait -e create,modify $CONFIG_FILES
    killall greenclip
done

