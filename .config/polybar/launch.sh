#!/bin/bash

CONFIG_FILES="$HOME/.config/polybar/config.ini"

trap "killall polybar" EXIT

echo "---" | tee -a /tmp/polybar1.log 

while true; do
    polybar -r main 2>&1 | tee -a /tmp/polybar1.log & disown
    inotifywait -e create,modify $CONFIG_FILES
    killall polybar
done

