#!/bin/bash

CONFIG_FILES="$HOME/.config/dunst/dunstrc"

trap "killall dunst" EXIT

echo "---" | tee -a /tmp/dunst.log 

while true; do
    dunst 2>&1 | tee -a /tmp/dunst.log & disown
    inotifywait -e create,modify $CONFIG_FILES
    killall dunst
done

