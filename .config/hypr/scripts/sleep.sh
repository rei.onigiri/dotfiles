#!/bin/bash

DISPLAY_OFF_TIME_SECONDS=150
DISPLAY_OFF_CMD="swaylock -f -i ~/Pictures/3440x1440/eva.jpg"
SLEEP_TIME_SECONDS=300
SLEEP_CMD="systemctl suspend"

swayidle -w timeout $DISPLAY_OFF_TIME_SECONDS "$DISPLAY_OFF_CMD" \
            timeout $SLEEP_TIME_SECONDS "$SLEEP_CMD" \
            before-sleep "$DISPLAY_OFF_CMD" &
