let g:WhichKey_ProcessUnknownMappings = "false"
let g:WhichKeyDesc_reload_config = "<leader><leader>r Reload IdeaVim"
nnoremap <leader><leader>r :so ~/.ideavimrc<CR>

" Quick stuff
let g:WhichKeyDesc_info = "<leader>i Info"
let g:WhichKeyDesc_info_quick_implementation = "<leader>ii Quick Implementation"
nmap <leader>ii <Action>(QuickImplementations)
let g:WhichKeyDesc_info_quick_type_definition = "<leader>it Quick Type Definition"
nmap <leader>it <Action>(QuickTypeDefinition)
let g:WhichKeyDesc_info_parameter_info = "<leader>ip Parameter Info"
nmap <leader>ip <Action>(ParameterInfo)
let g:WhichKeyDesc_info_javadoc = "<leader>ip Javadoc"
nmap <leader>ij <Action>(QuickJavaDoc)
let g:WhichKeyDesc_info_error = "<leader>ip Error"
nmap <leader>ie <Action>(ShowErrorDescription)
let g:WhichKeyDesc_info_structure = "<leader>is Structure"
nmap <leader>is <Action>(FileStructurePopup)

" Goto
let g:WhichKeyDesc_goto = "<leader>g Goto"
let g:WhichKeyDesc_goto_declaration = "<leader>gd Goto declaration"
map <leader>gd <Action>(GotoDeclaration)
let g:WhichKeyDesc_goto_type_declaration = "<leader>gt Goto type declaration"
map <leader>gt <Action>(GotoTypeDeclaration)
let g:WhichKeyDesc_goto_implementation = "<leader>gi Goto implementation"
map <leader>gi <Action>(GotoImplementation)
let g:WhichKeyDesc_goto_super_method = "<leader>gs Goto super method"
map <leader>gs <Action>(GotoSuperMethod)

" Find
let g:WhichKeyDesc_find_class = "<leader>fc Find class"
map <leader>fc <Action>(GotoClass)
let g:WhichKeyDesc_find_file = "<leader>ff Find file"
map <leader>ff <Action>(GotoFile)
let g:WhichKeyDesc_find_text = "<leader>ft Find text"
map <leader>ft <Action>(Find)
let g:WhichKeyDesc_find_all_texts = "<leader>fT Find all texts"
map <leader>fT <Action>(FindInPath)
let g:WhichKeyDesc_replace_text = "<leader>fr Replace text"
map <leader>fr <Action>(Replace)
let g:WhichKeyDesc_replace_text = "<leader>fR Replace all texts"
map <leader>fR <Action>(ReplaceInPath)

" Run stuff
let g:WhichKeyDesc_run = "<leader>r Run"
let g:WhichKeyDesc_run_class = "<leader>rc Run class"
map <leader>rc <Action>(RunClass)
let g:WhichKeyDesc_run_choose_configuration = "<leader>rC Choose run configuration"
map <leader>rC <Action>(ChooseRunConfiguration)
let g:WhichKeyDesc_debug_class = "<leader>rd Debug class"
map <leader>rd <Action>(DebugClass)
let g:WhichKeyDesc_debug_choose_configuration = "<leader>rD Choose debug configuration"
map <leader>rD <Action>(ChooseDebugConfiguration)
let g:WhichKeyDesc_run_rerun = "<leader>rr Rerun"
map <leader>rr <Action>(Rerun)
let g:WhichKeyDesc_run_stop = "<leader>rs Stop run"
map <leader>rs <Action>(Stop)

" Reformat
let g:WhichKeyDesc_reformat_code_full = "<leader>tfc Format full"
map <leader>tff <Action>(ReformatCode)<Action>(OptimizeImports)
let g:WhichKeyDesc_reformat_code = "<leader>tfc Format code"
map <leader>tfc <Action>(ReformatCode)
let g:WhichKeyDesc_reformat_imports = "<leader>tfi Optimize imports"
map <leader>tfi <Action>(OptimizeImports)

" Clojure
let g:WhichKeyDesc_find_namespace = "<leader>fn Find namespace"
map <leader>fn <Action>(GotoNamespace)
let g:WhichKeyDesc_cursive_load_file = "<leader>crl Load file"
map <leader>crl <Action>(:cursive.repl.actions/load-file)
let g:WhichKeyDesc_cursive_sync_files = "<leader>crL Sync files"
map <leader>crL <Action>(:cursive.repl.actions/sync-files)
let g:WhichKeyDesc_cursive_send_form_full = "<leader>crF Send form (full)"
map <leader>crF <Action>(:cursive.repl.actions/run-top-sexp)
let g:WhichKeyDesc_cursive_send_form_before_caret = "<leader>crf Send form (before caret)"
map <leader>crf <Action>(:cursive.repl.actions/run-last-sexp)
let g:WhichKeyDesc_cursive_slurp_forwards = "<leader>cfs Slurp forwards"
map <leader>cfs <Action>(:cursive.actions.paredit/slurp-forwards)
let g:WhichKeyDesc_cursive_slurp_backwards = "<leader>cfS Slurp backwards"
map <leader>cfS <Action>(:cursive.actions.paredit/slurp-backwards)
let g:WhichKeyDesc_cursive_barf_forwards = "<leader>cfb Barf forwards"
map <leader>cfb <Action>(:cursive.actions.paredit/barf-forwards)
let g:WhichKeyDesc_cursive_barf_backwards = "<leader>cfB Barf backwards"
map <leader>cfB <Action>(:cursive.actions.paredit/barf-backwards)
let g:WhichKeyDesc_cursive_raise = "<leader>cfr Raise"
map <leader>cfr <Action>(:cursive.actions.paredit/raise)
let g:WhichKeyDesc_cursive_splice = "<leader>cfp Splice"
map <leader>cfp <Action>(:cursive.actions.paredit/splice)
let g:WhichKeyDesc_cursive_join = "<leader>cfj Join"
map <leader>cfj <Action>(:cursive.actions.paredit/join)
let g:WhichKeyDesc_cursive_move_form_up = "<leader>cfh Move form up"
map <leader>cfh <Action>(:cursive.actions.paredit/move-form-up)
let g:WhichKeyDesc_cursive_move_form_down = "<leader>cfl Move form down"
map <leader>cfl <Action>(:cursive.actions.paredit/move-form-down)

