require('which-key').register({
    w = {
        name = 'Window manipulation',
        s = {
            name = 'Split window',
            h = { 'Split horizontally' },
            s = { 'Split vertically' },
        },
    },
}, { prefix = '<leader>' })

-- Tab navigation
if vim.loop.os_uname().sysname == 'Darwin' then
    vim.keymap.set('n', '<C-F7>', '<CMD>:bprev<CR>')
    vim.keymap.set('n', '<C-F8>', '<CMD>:bnext<CR>')
else
    vim.keymap.set('n', '<A-[>', '<CMD>:bprev<CR>')
    vim.keymap.set('n', '<A-]>', '<CMD>:bnext<CR>')
end

