require('which-key').register({
    j = { 'Hop to character' }
}, { prefix = '<leader>' })
vim.keymap.set('n', '<leader>j', '<CMD>:HopChar1<CR>')
vim.keymap.set('v', '<leader>j', '<CMD>:HopChar1<CR>')
