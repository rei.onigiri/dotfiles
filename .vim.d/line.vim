if has('ide')
    let g:WhichKeyDesc_line = "<leader>l Line manipulation"
    let g:WhichKeyDesc_line_add = "<leader>la Add line"
    let g:WhichKeyDesc_line_add_below = "<leader>la Add new line below"
    let g:WhichKeyDesc_line_delete = "<leader>ld Delete line"
    let g:WhichKeyDesc_line_delete_empty = "<leader>lde Delete empty line"
    let g:WhichKeyDesc_line_delete_empty_up = "<leader>ldek Delete empty line upward"
    let g:WhichKeyDesc_line_delete_empty_down = "<leader>ldej Delete empty line downward"
    let g:WhichKeyDesc_line_move = "<leader>lm Move line"
    let g:WhichKeyDesc_line_move_to = "<leader>lmt Move line to"
    let g:WhichKeyDesc_line_move_up = "<leader>lmk Move line up"
    let g:WhichKeyDesc_line_move_down = "<leader>lmj Move line down"
    let g:WhichKeyDesc_line_copy = "<leader>lc Copy line down"
    let g:WhichKeyDesc_line_comment = "<leader>lC Comment line"

    nmap <leader>lC <Action>(CommentByLineComment)
    vmap <leader>lC <Action>(CommentByBlockComment)
else
    source ~/.vim.d/line.lua
endif

" Delete empty line
nnoremap <leader>ldek d?.<CR>:noh<CR>
nnoremap <leader>ldej d/.<CR>:noh<CR>
nnoremap <Leader>laj <ESC>a<CR><ESC>

" Move line
nnoremap <silent> <leader>lmt :m 
vnoremap <silent> <leader>lmt :m 
nnoremap <silent> <leader>lmk :m -
vnoremap <silent> <leader>lmk :m -
nnoremap <silent> <leader>lmj :m +
vnoremap <silent> <leader>lmj :m +

" Move line up/down
nnoremap <C-S-K> :m .-2<CR>
nnoremap <C-S-J> :m .+1<CR>
vnoremap <C-S-K> :m '<-2<CR>gv=gv
vnoremap <C-S-J> :m '>+1<CR>gv=gv
 
" New line
nnoremap <C-Enter> <ESC>o<ESC>
nnoremap <S-Enter> <ESC>O<ESC>

" Duplicate
nnoremap <leader>lc ddup
vnoremap <leader>lc yP
