 if has('ide')
    let g:WhichKeyDesc_nerdtree_toggle = "<leader>tp Open NerdTree"
    let g:WhichKeyDesc_nerdtree_find = "<leader>tP Find file in NerdTree"

    nnoremap <leader>tp :NERDTreeToggle<CR>
    nnoremap <leader>tP :NERDTreeFind<CR>
else
    source ~/.vim.d/nerdtree.lua
endif

