require("which-key").register({
	f = {
		name = "Find",
		f = { "Find files" },
		t = { "Find texts in current file" },
		T = { "Find texts in all files" },
	},
}, { prefix = "<leader>" })

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>ff", "<CMD>Telescope find_files hidden=true no_ignore=true<CR>")
vim.keymap.set("n", "<leader>ft", function()
	return builtin.current_buffer_fuzzy_find()
end, {})
vim.keymap.set('n', '<leader>fT', function()
    return builtin.live_grep({
        additional_args={"--hidden"}
    })
end, {})

