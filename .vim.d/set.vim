if has('ide')
    set NERDTree
    set easymotion
    set surround
    set clipboard+=ideaput
    set visualbell
    set noerrorbells
    set which-key
    set notimeout
else
    set termguicolors
    set showmatch               " show matching 
    set tabstop=4               " number of columns occupied by a tab 
    set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
    set expandtab               " converts tabs to white space
    set shiftwidth=4            " width for autoindents
    set autoindent              " indent a new line the same amount as the line just typed
    set wildmode=longest,list   " get bash-like tab completions
    set cc=120                  " set an 120 column border for good coding style
    syntax on                   " syntax highlighting
    set mouse=a                 " enable mouse click
    set ttyfast                 " Speed up scrolling in Vim
    set noswapfile              " disable creating swap file
    set backupdir=~/.cache/vim  " Directory to store backup files.
    set cursorline
endif

set ignorecase
set smartcase
set hlsearch
set incsearch
set number
set clipboard=unnamedplus
set rnu
set ttyfast

