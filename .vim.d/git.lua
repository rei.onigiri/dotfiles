require('which-key').register({
    ['<leader>tg'] = { 'Git tool window' },
})
vim.keymap.set('n', '<leader>tg', '<CMD>:LazyGit<CR>')
