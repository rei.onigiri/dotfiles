if has('ide')
    nmap <leader>tg <Action>(ActivateCommitToolWindow)
else
    source ~/.vim.d/git.lua
endif

