nnoremap <Space> <Nop>
let mapleader=" "

source ~/.vim.d/set.vim

if !has('ide')
    set listchars=space:·,tab:->\ 
    set list
    source ~/.vim.d/main.lua
endif

source ~/.vim.d/easymotion.vim
source ~/.vim.d/git.vim
source ~/.vim.d/line.vim
source ~/.vim.d/movement.vim
source ~/.vim.d/multi_cursor.vim
source ~/.vim.d/nerdtree.vim
source ~/.vim.d/text.vim
source ~/.vim.d/windows_panes_tabs.vim
source ~/.vim.d/search.vim

if has('ide')
    source ~/.vim.d/intellij.vim
endif

