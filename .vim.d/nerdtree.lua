require('which-key').register({
    ['<leader>tp'] = { 'Open Neotree' },
    ['<leader>tP'] = { 'Open current file in Neotree' },
})
vim.keymap.set('n', '<leader>tp', '<CMD>:Neotree<CR>')
vim.keymap.set('n', '<leader>tP', '<CMD>:Neotree reveal<CR>')
