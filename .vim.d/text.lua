require('which-key').register({
    t = {
        name = 'Text manipulation',
        s = {
            name = 'Text surrouund',
            a = { 'Add text surround' },
            c = { 'Change text surround' },
            d = { 'Delete text surround' },
        },
        d = {
            name = 'Delete text',
            b = { 'Delete text inside character X' },
            l = { 'Delete text until character X' },
            h = { 'Delete text until character X (left)' },
        },
    },
}, { prefix = '<leader>' })

vim.keymap.set({ 'n', 'v' }, '<C-w>', '<Plug>(expand_region_expand)')
vim.keymap.set({ 'n', 'v' }, '<C-S-w>', '<Plug>(expand_region_shrink)')

vim.g.caser_no_mappings = 1
vim.keymap.set({ 'n' }, '<leader>tcc', '<Plug>CaserCamelCase')
vim.keymap.set({ 'v' }, '<leader>tcc', '<Plug>CaserVCamelCase')
vim.keymap.set({ 'n' }, '<leader>tcp', '<Plug>CaserMixedCase')
vim.keymap.set({ 'v' }, '<leader>tcp', '<Plug>CaserVMixedCase')
vim.keymap.set({ 'n' }, '<leader>tcs', '<Plug>CaserSnakeCase')
vim.keymap.set({ 'v' }, '<leader>tcs', '<Plug>CaserVSnakeCase')
vim.keymap.set({ 'n' }, '<leader>tcS', '<Plug>CaserUpperCase')
vim.keymap.set({ 'v' }, '<leader>tcS', '<Plug>CaserVUpperCase')
vim.keymap.set({ 'n' }, '<leader>tct', '<Plug>CaserTitleCase')
vim.keymap.set({ 'v' }, '<leader>tct', '<Plug>CaserVTitleCase')
vim.keymap.set({ 'n' }, '<leader>tc ', '<Plug>CaserSpaceCase')
vim.keymap.set({ 'v' }, '<leader>tc ', '<Plug>CaserVSpaceCase')
vim.keymap.set({ 'n' }, '<leader>tck', '<Plug>CaserKebabCase')
vim.keymap.set({ 'v' }, '<leader>tck', '<Plug>CaserVKebabCase')
vim.keymap.set({ 'n' }, '<leader>tcK', '<Plug>CaserKebabCapsCase')
vim.keymap.set({ 'v' }, '<leader>tcK', '<Plug>CaserVKebabCapsCase')
vim.keymap.set({ 'n' }, '<leader>tcd', '<Plug>CaserDotCase')
vim.keymap.set({ 'v' }, '<leader>tcd', '<Plug>CaserVDotCase')
vim.keymap.set({ 'n' }, '<leader>tcD', '<Plug>CaserDotCapsCase')
vim.keymap.set({ 'v' }, '<leader>tcD', '<Plug>CaserVDotCapsCase')

vim.keymap.set('n', '<leader>tfc', '<CMD>:Format<CR>')

