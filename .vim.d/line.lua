require("which-key").register({
	l = {
		name = "Line manipulation",
		a = {
			name = "Add line",
			j = { "Add new line below" },
		},
		d = {
			name = "Delete line",
			e = {
				name = "Delete empty line",
				k = { "Delete empty line above" },
				j = { "Delete empty line below" },
			},
		},
		m = {
			name = "Move line",
			t = { "Move line to X" },
			k = { "Move line up" },
			j = { "Move line down" },
		},
		c = { "Copy line down" },
        C = { "Toggle comment" },
	},
}, { prefix = "<leader>" })

local api = require("Comment.api")
vim.keymap.set("n", "<leader>lC", api.call("toggle.linewise.current", "g@$"), { expr = true })
vim.keymap.set("v", "<leader>lC", api.call("toggle.linewise", "g@"), { expr = true })
