if has('ide')
    let g:WhichKeyDesc_tab_prev = "<A-[>"
    let g:WhichKeyDesc_tab_next = "<A-]>"
    let g:WhichKeyDesc_pane_left = "<A-h>"
    let g:WhichKeyDesc_pane_down = "<A-j>"
    let g:WhichKeyDesc_pane_up = "<A-k>"
    let g:WhichKeyDesc_pane_right = "<A-l>"
    let g:WhichKeyDesc_window = "<leader>w Windows manipulation"
    let g:WhichKeyDesc_window_split = "<leader>w Split window"
    let g:WhichKeyDesc_window_split_vertically = "<leader>wsv Split vertically"
    let g:WhichKeyDesc_window_split_horizontal = "<leader>wsh Split horizontally"
    let g:WhichKeyDesc_window_split_unsplit = "<leader>wu Unsplit"
    let g:WhichKeyDesc_window_split_move_editor_to_opposite = "<leader>wm Move editor to opposite tab group"
    
    " Window splits
    "map <leader>wu <Action>(Unsplit)
    "map <leader>wm <Action>(MoveEditorToOppositeTabGroup)

    " Tab navigation
    nnoremap <A-[> :tabprev<CR>
    nnoremap <A-]> :tabnext<CR>
    nnoremap <C-F7> :tabprev<CR>
    nnoremap <C-F8> :tabnext<CR>
else
    source ~/.vim.d/windows_panes_tabs.lua
endif

" Pane split
nnoremap <leader>wsh <ESC>:split<CR>
nnoremap <leader>wsv <ESC>:vsplit<CR>

" Pane navigation
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l
nnoremap <C-F9> <C-w>h
nnoremap <C-F10> <C-w>j
nnoremap <C-F11> <C-w>k
nnoremap <C-F12> <C-w>l

" Move pane
nnoremap <leader>wmh <C-w>H
nnoremap <leader>wmj <C-w>J
nnoremap <leader>wmk <C-w>K
nnoremap <leader>wml <C-w>L
