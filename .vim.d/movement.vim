if has('ide')
    let g:WhichKeyDesc_move_up = "<C-k>"
    let g:WhichKeyDesc_move_down = "<C-j>"
    let g:WhichKeyDesc_editor_up = "<C-h>"
    let g:WhichKeyDesc_editor_down = "<C-l>"

    " Move up/down
    nnoremap <C-k> <C-U>
    nnoremap <C-j> <C-D>
    vnoremap <C-k> <C-U>
    vnoremap <C-j> <C-D>

    " Move up/down menu selection
    imap <C-h> <Action>(EditorUp)
    imap <C-l> <Action>(EditorDown)
else
    source ~/.vim.d/movement.lua
endif
