if has('ide')
    let g:WhichKeyDesc_text = "<leader>t Text Manipulation"
    let g:WhichKeyDesc_text_surround = "<leader>ts Surround text with character"
    let g:WhichKeyDesc_text_surround_change = "<leader>tsc Change text surrounding"
    let g:WhichKeyDesc_text_surround_delete = "<leader>tsd Delete text surrounding"
    let g:WhichKeyDesc_text_surround_add = "<leader>tsa Add text surround"
    let g:WhichKeyDesc_text_delete = "<leader>td Delete text"
    let g:WhichKeyDesc_text_delete_inside = "<leader>tdb Delete text inside character)"
    let g:WhichKeyDesc_text_delete_right = "<leader>tdl Delete text until character (rightward)"
    let g:WhichKeyDesc_text_delete_left = "<leader>tdh Delete text until character (leftward)"
    let g:WhichKeyDesc_select_word = "<C-w>"
    let g:WhichKeyDesc_unselect_word = "<C-S-W>"

    " Text selection
    nmap <C-w> <Action>(EditorSelectWord)
    vmap <C-w> <Action>(EditorSelectWord)
    vmap <C-S-W> <Action>(EditorUnSelectWord)

    " String case manipulation
    nmap <leader>tcc <Action>(StringManipulation.ToCamelCase)
    vmap <leader>tcc <Action>(StringManipulation.ToCamelCase)
    nmap <leader>tcp <Action>(StringManipulation.ToPascalCase)
    vmap <leader>tcp <Action>(StringManipulation.ToPascalCase)
    nmap <leader>tcs <Action>(StringManipulation.ToSnakeCase)
    vmap <leader>tcs <Action>(StringManipulation.ToSnakeCase)
    nmap <leader>tcS <Action>(StringManipulation.ToSnakeCase)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
    vmap <leader>tcS <Action>(StringManipulation.ToSnakeCase)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
    nmap <leader>tck <Action>(StringManipulation.ToKebabCase)
    vmap <leader>tck <Action>(StringManipulation.ToKebabCase)
    nmap <leader>tcK <Action>(StringManipulation.ToKebabCase)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
    vmap <leader>tcK <Action>(StringManipulation.ToKebabCase)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
    nmap <leader>tcd <Action>(StringManipulation.ToCamelCase)<Action>(StringManipulation.ToDotStyleAction)
    vmap <leader>tcd <Action>(StringManipulation.ToCamelCase)<Action>(StringManipulation.ToDotStyleAction)
    nmap <leader>tcD <Action>(StringManipulation.ToCamelCase)<Action>(StringManipulation.ToDotStyleAction)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
    vmap <leader>tcD <Action>(StringManipulation.ToCamelCase)<Action>(StringManipulation.ToDotStyleAction)<Action>(osmedile.intellij.stringmanip.ToUpperCaseAction)
else
    source ~/.vim.d/text.lua
endif

nmap <Leader>tsc cs
nmap <Leader>tsd ds
nmap <Leader>tsa ysiw
vmap <Leader>tsa S
nmap <silent> <Leader>tdb di
nmap <silent> <Leader>tdl dt
nmap <silent> <Leader>tdh dT

