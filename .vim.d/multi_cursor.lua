if vim.loop.os_uname().sysname == 'Darwin' then
    vim.keymap.set('n', '<S-F11>', '<Plug>(VM-Add-Cursor-Up)')
    vim.keymap.set('n', '<S-F10>', '<Plug>(VM-Add-Cursor-Down)')
else
    -- Alt-Shift-K
    vim.keymap.set('n', '<space>$k', '<Plug>(VM-Add-Cursor-Up)')
    -- Alt-Shift-J
    vim.keymap.set('n', '<space>$j', '<Plug>(VM-Add-Cursor-Down)')
end

