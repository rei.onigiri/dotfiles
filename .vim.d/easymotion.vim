if has('ide')
    let g:WhichKeyDesc_easymotion = "<leader>j Jump"
    nmap <leader>j <Plug>(easymotion-bd-f)
else
    source ~/.vim.d/easymotion.lua
endif

