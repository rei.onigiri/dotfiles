if has('ide')
    function! IsLinux()
        return substitute(system('uname'), '\n', '', '') != 'Darwin'
    endfunction

    echo hello
    if IsLinux()
        nmap <A-S-K> <Action>(EditorCloneCaretAbove)
        nmap <A-S-J> <Action>(EditorCloneCaretBelow)
    else
        nmap <S-F10> <Action>(EditorCloneCaretBelow)
        nmap <S-F11> <Action>(EditorCloneCaretAbove)
    endif
else
    source ~/.vim.d/multi_cursor.lua
endif

